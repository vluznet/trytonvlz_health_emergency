# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['HealthConfiguration']


class HealthConfiguration(metaclass=PoolMeta):
    __name__ = 'gnuhealth.sequences'
    emergency_sequence = fields.Many2One('ir.sequence', 'Emergency Sequence',
        required=True, domain=[('code', '=', 'gnuhealth.patient')])
    location_emergency_default = fields.Many2One('stock.location',
        'Location Emergency Default', domain=[
            ('type', '=', 'warehouse')
        ])
