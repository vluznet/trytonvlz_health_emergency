from datetime import datetime, timedelta

from trytond.model import ModelView, ModelSQL, fields, Unique, Workflow
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Not, Bool, Or
from trytond.report import Report
from trytond.wizard import Wizard, StateTransition

__all__ = [
    'Emergency', 'VitalSigns', 'ToxicHabits', 'NoteList', 'Interconsultation',
    'Lab', 'EmergencyProcedures', 'ImagingTestRequest', 'MedicalReleaseReport',
    'ReferredPatientReport', 'EmergencyMedicalHistory', 'EmergencyMedication',
    'CreateEmergencyShipment', 'Move', 'InpatientRegistration',
]

TRIAGE_LEVEL = [
    ('', ''),
    ('level1', 'Level I'),
    ('level2', 'Level II'),
    ('level3', 'Level III'),
    ('level4', 'Level IV'),
    ('level5', 'Level V'),
]

STATUS = [
    ('', ''),
    ('conscious', 'Conscious'),
    ('unconscious', 'Unconscious'),
    ('coma', 'Coma'),
    ('dead', 'Dead'),
]

TRANSFER_ROOM = [
    ('', ''),
    ('general_operating_room', 'General Operating Room'),
    ('emergency_operating_room', 'Emergency Operating Room'),
    ('observation', 'Observation'),
    ('triaje1', 'Triaje 1'),
    ('triaje2', 'Triaje 2'),
    ('pre-emergency', 'Pre Emergency'),
    ('curettage', 'Curettage IV'),
    ('external referral', 'External Referral'),
]

PAIN_SCALE = [
    ('', ''),
    ('without pain', 'Without Pain'),
    ('slight pain', 'Slight Pain'),
    ('moderate pain', 'Moderate Pain'),
    ('severe pain', 'Severe Pain'),
    ('very severe pain', 'Very Severe Pain'),
    ('maximum pain', 'Maximum Pain'),
]

TOXIC_HABITS = [
    ('', ''),
    ('coffee', 'Coffee'),
    ('cigarette', 'Cigarette'),
    ('tobacco', 'Tobacco'),
    ('pipa', 'Pipa'),
    ('alcohol', 'Alcohol'),
]

DISCHARGE_DESTINATION = [
    ('', ''),
    ('house', 'House'),
    ('hospitalization', 'Hospitalization'),
    ('transfer to general operating room', 'Transfer to general operating room'),
    ('transfer to other center', 'Transfer to other center'),
    ('resuscitation room', 'Resuscitation room'),
    ('rehydration room', 'Rehydration room'),
    ('post-emergency room', 'Post-emergency room'),
    ('curettage room', 'Curettage room'),
    ('nebulization room', 'Nebulization room'),
    ('external referral', 'External referral'),
    ('morgue', 'Morgue'),
    ('leakage', 'Leakage'),
    ('consultation', 'Consultation'),
]


class Emergency(Workflow, ModelSQL, ModelView):
    'Emergency'
    __name__ = 'gnuhealth.emergency'
    STATES = {
        'readonly': Eval('state').in_(['cancelled', 'discharged'])
    }
    STATES_ADMITTED = {
        'readonly': Eval('state') != 'draft',
    }
    number = fields.Char('Number', readonly=True, help="Sequence", select=True)
    puid = fields.Function(fields.Char('Nec'), 'get_puid',
        searcher='search_puid')
    patient = fields.Many2One('gnuhealth.patient', 'Patient',
        required=True, select=True, states=STATES_ADMITTED)
    registration_date = fields.DateTime('Registration date',
        required=True, select=True, states=STATES_ADMITTED)
    location = fields.Many2One('stock.location', 'Location',
        states=STATES_ADMITTED, domain=[('type', '=', 'warehouse')])
    admission_type = fields.Selection([
        (None, ''),
        ('routine', 'Routine'),
        ('maternity', 'Maternity'),
        ('elective', 'Elective'),
        ('urgent', 'Urgent'),
        ('emergency', 'Emergency'),
        ], 'Admission type', required=True, select=True,
        states=STATES_ADMITTED)
    attending_medical_prof = fields.Many2One('gnuhealth.healthprofessional',
        'Attending Medical Prof.',  states=STATES_ADMITTED)
    presumptive_diagnosis = fields.Char('Presumptive Diagnosis')
    admission_reason = fields.Many2One('gnuhealth.pathology',
        'Reason for Admission', help="Reason for Admission", select=True,
        states=STATES)
    bed = fields.Many2One('gnuhealth.hospital.bed', 'Hospital Bed',
        states={
          'readonly': Or(
                Eval('state') == 'admitted',
                Bool(Eval('number')),
            )}, depends=['number'])
    state = fields.Selection([
        (None, ''),
        ('draft', 'Draft'),
        ('cancelled', 'Cancelled'),
        ('admitted', 'Admitted'),
        ('discharged', 'Discharged'),
        ], 'Status', select=True, readonly=True)
    discharged_by = fields.Many2One(
        'gnuhealth.healthprofessional', 'Discharged by', readonly=True,
        help="Health Professional that discharged the patient", states=STATES)
    institution = fields.Many2One('gnuhealth.institution', 'Institution',
        readonly=True)
    referred_by = fields.Many2One('gnuhealth.institution', 'Refferd By',
        states=STATES)
    arrival_status = fields.Selection(STATUS, 'Arrival Status', states=STATES_ADMITTED)
    triage_level = fields.Selection(TRIAGE_LEVEL, 'Triage Level', states=STATES_ADMITTED)
    triage_level_string = triage_level.translated('triage_level')
    pain_scale = fields.Selection(PAIN_SCALE, 'Pain Scale', states=STATES)
    pain_scale_string = pain_scale.translated('pain_scale')
    transfer_rooms = fields.Selection(TRANSFER_ROOM, 'Transfer Room',
        states=STATES)
    transfer_rooms_string = transfer_rooms.translated('transfer_rooms')
    vital_signs = fields.One2Many('gnuhealth.emergency.vital_signs',
        'emergency', 'Vital Signs', states=STATES)
    arrival_status_string = arrival_status.translated('arrival_status')
    emergency_detail = fields.Char('Emergency Detail', states=STATES)
    disease_history = fields.Text('Disease History', states=STATES)
    # Second Page Antecedent / History
    childhood_history = fields.Text('Childhood', states=STATES)
    adolescence_history = fields.Text('Adolescence', states=STATES)
    adulthood_history = fields.Text('Adulthood', states=STATES)
    family_history = fields.Text('Family History', states=STATES)
    obstetrics_gynecology = fields.Boolean('Obstetrics Gynecology',
        states=STATES)
    date_last_menstruation = fields.Date('FUM', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    births = fields.Integer('P', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    abortions = fields.Integer('A', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    caesarean = fields.Integer('C', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    pregnancies = fields.Integer('E', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology')),
    })
    t_gestations = fields.Integer('T.Gestations', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    common_medication = fields.Text('Common Medication', states=STATES)
    blood_transfusion = fields.Text('Blood Transfusion', states=STATES)
    toxic_habits = fields.One2Many('gnuhealth.emergency.toxic_habits',
        'emergency', 'Toxic Habits', states=STATES)
    other_habits = fields.Char('Other Habits', states=STATES)
    medications_allergy = fields.Text('Medications Allegy', states=STATES)
    food_allergy = fields.Text('Food Allegy', states=STATES)
    other_allergy = fields.Text('Other Allegy', states=STATES)
    surgical_history = fields.Text('Surgical', states=STATES)
    trauma_history = fields.Text('Trauma', states=STATES)
    # Third Page Physical State
    physical_state = fields.Text('Physical', states=STATES)
    neurological_state = fields.Text('Neurological', states=STATES)
    head_neck_state = fields.Text('Head Neck', states=STATES)
    thorax_state = fields.Text('Thorax', states=STATES)
    heart_state = fields.Text('Heart', states=STATES)
    lungs_state = fields.Text('Lungs', states=STATES)
    abdomen_state = fields.Text('Abdomen', states=STATES)
    pelvis_state = fields.Text('Pelvis', states=STATES)
    vaginal_examination = fields.Text('Vaginal Examination', states=STATES)
    extremities_state = fields.Text('Extremities', states=STATES)
    other_state = fields.Text('Other State', states=STATES)
    disability = fields.Boolean('Disability', states=STATES)
    disability_description = fields.Text('Disability Description', states={
        'required': Bool(Eval('disability')),
        'invisible': ~Bool(Eval('disability'))
    })
    # Four Page Evaluations and interconsultations
    note_list = fields.One2Many('gnuhealth.emergency.note_list',
        'emergency', 'Note List', states=STATES)
    interconsultations = fields.One2Many(
        'gnuhealth.emergency.interconsultations',
        'emergency', 'Interconsultations', states=STATES)
    # Fifth Page Medications / Labs
    medications = fields.One2Many('gnuhealth.emergency.medication',
        'emergency', 'Medications', states=STATES)
    labs = fields.One2Many('gnuhealth.lab', 'emergency', 'Labs', states=STATES)
    imaging_requests = fields.One2Many(
        'gnuhealth.imaging.test.request',
        'emergency', 'Imagin Request', states=STATES)
    # Sixth Page Medications / Labs
    procedures = fields.One2Many('gnuhealth.emergency.procedures',
        'emergency', 'Procedures', states=STATES)
    diagnosis_1 = fields.Many2One('gnuhealth.pathology', 'Diagnosis 1',
        states={
            'readonly': Eval('state').in_(['discharged', 'cancelled']),
            'required': Eval('state') == 'discharged',
        }, depends=['state'])
    diagnosis_2 = fields.Many2One('gnuhealth.pathology', 'Diagnosis 2',
        states=STATES)
    diagnosis_3 = fields.Many2One('gnuhealth.pathology', 'Diagnosis 3',
        states=STATES)
    discharge_condition = fields.Char('Discharge Condition', states=STATES)
    state_discharge = fields.Selection(STATUS, 'State Discharge', states=STATES)
    state_discharge_string = state_discharge.translated('state_discharge')
    discharge_destination = fields.Selection(DISCHARGE_DESTINATION,
        'Discharge Destination', depends=['state'], states={
            'readonly': Eval('state').in_(['discharged', 'cancelled']),
            'required': Eval('state') == 'discharged',
        })
    discharge_destination_string = discharge_destination.translated(
        'discharge_destination')
    discharge_attending_physician = fields.Many2One(
        'gnuhealth.healthprofessional', 'Discharge Attending Physician',
        states=STATES)
    referred_patient = fields.Boolean('Referred Patient', states=STATES)
    referred_diagnostic = fields.Char('Referred Diagnostic', states={
        'required': Bool(Eval('referred_patient')),
        'invisible': ~Bool(Eval('referred_patient'))
    })
    information_referred_diagnostic = fields.Char('Who Referred Diagnostic',
        states={
            'required': Bool(Eval('referred_patient')),
            'invisible': ~Bool(Eval('referred_patient'))
        }
    )
    position = fields.Char('Position', states={
        'required': Bool(Eval('referred_patient')),
        'invisible': ~Bool(Eval('referred_patient'))
    })
    receiving_center = fields.Char('Receivin Center', states={
        'required': Bool(Eval('referred_patient')),
        'invisible': ~Bool(Eval('referred_patient'))
    })
    information_authorized_reception = fields.Char('Who autorized The reception',
        states={
            'required': Bool(Eval('referred_patient')),
            'invisible': ~Bool(Eval('referred_patient'))
        }
    )
    position_autorized = fields.Char('Position Autorized', states={
        'required': Bool(Eval('referred_patient')),
        'invisible': ~Bool(Eval('referred_patient'))
    })
    duration_referred_center = fields.Char('Time In Center Referred', states={
        'required': Bool(Eval('referred_patient')),
        'invisible': ~Bool(Eval('referred_patient'))
    })
    condition_of_referred = fields.Text('Condition Determined The referred',
        states={
            'required': Bool(Eval('referred_patient')),
            'invisible': ~Bool(Eval('referred_patient'))
        })
    where_referred_ceas = fields.Many2One('gnuhealth.institution',
        'Where Referred', states={
            'required': Bool(Eval('referred_patient')),
            'invisible': ~Bool(Eval('referred_patient'))
        })
    discharge_date = fields.Date('Discharge Date')
    petition_release = fields.Boolean('Petition Release', states={
        'readonly': Or(
           Eval('state') == 'admitted',
           Bool(Eval('petition_party')),
           Bool(Eval('petition_patient'))
        )
    })
    petition_patient = fields.Boolean('Petition of Patient', states={
        'required': Bool(Eval('petition_release')),
        'invisible': ~Bool(Eval('petition_release')),
        'readonly': Bool(Eval('petition_party'))
    })
    petition_party = fields.Boolean('Petition of Party', states={
        'required': Bool(Eval('petition_release')),
        'invisible': ~Bool(Eval('petition_release')),
        'readonly': Bool(Eval('petition_patient'))
    })
    name_petition_party = fields.Char('Name of party', states={
        'required': Bool(Eval('petition_party')),
        'invisible': ~Bool(Eval('petition_party'))
    })
    document_petition_party = fields.Char('Number Id of Party', states={
        'required': Bool(Eval('petition_party')),
        'invisible': ~Bool(Eval('petition_party'))
    })
    address_petition_party = fields.Char('Adress of Party', states={
        'required': Bool(Eval('petition_party')),
        'invisible': ~Bool(Eval('petition_party'))
    })

    @classmethod
    def __setup__(cls):
        super(Emergency, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_unique', Unique(t, t.number),
                'The Emergency code already exists'),
        ]
        cls._error_messages.update({
            'bed_is_not_available': 'Bed is not available',
            'emergency_sequence_missing': 'Sequence Emergency is missing!',
        })
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'admitted',
            },
            'admit': {
                'invisible': Eval('state') != 'draft'
            },
            'cancel': {
                'invisible': Eval('state') != 'admitted',
            },
            'discharge': {
                'invisible': Eval('state') != 'admitted',
            },
        })
        cls._transitions |= set((
            ('draft', 'admitted'),
            ('admitted', 'discharged'),
            ('admitted', 'cancelled'),
            ('admitted', 'draft'),
            ('cancelled', 'draft'),
        ))

    def get_puid(self, name):
        return self.patient.puid

    def get_id_number(self, name):
        return self.patient.name.id_number

    def set_number(self):
        if self.number:
            return
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Configuration = pool.get('gnuhealth.sequences')
        company_id = Transaction().context.get('company')
        configurations = Configuration.search([
            ('company', "=", company_id)
        ])
        if not configurations:
            self.raise_user_error('emergency_sequence_missing',)
            return

        configuration = configurations[0]
        if not configuration.emergency_sequence:
            self.raise_user_error('emergency_sequence_missing',)
        seq = configuration.emergency_sequence.id
        self.write([self], {'number': Sequence.get_id(seq)})

    @classmethod
    def search_puid(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('patient.puid', clause[1], value))
        return res

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('admitted')
    def admit(cls, records):
        for record in records:
            record.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('discharged')
    def discharge(cls, records):
        for rec in records:
            if rec.discharge_destination == 'hospitalization':
                rec.create_hospitalization()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    def admission(cls, records):
        pass

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_registration_date():
        return datetime.now()

    @staticmethod
    def default_location():
        Configuration = Pool().get('gnuhealth.sequences')
        company_id = Transaction().context.get('company')
        configurations = Configuration.search([
            ('company', "=", company_id)
        ])
        if configurations and configurations[0].location_emergency_default:
            return configurations[0].location_emergency_default.id

    @staticmethod
    def default_institution():
        company_id = Transaction().context.get('company')
        Institution = Pool().get('gnuhealth.institution')
        institutions = Institution.search([
            ('name', "=", company_id)
        ])
        if institutions:
            return institutions[0].id

    def generate_shipment(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        Date = pool.get('ir.date')
        to_create = []
        moves = self.get_stock_moves(self.medications)
        to_create.append({
            'planned_date': Date.today(),
            'company': Transaction().context.get('company'),
            'customer': self.patient.name.id,
            'reference': self.number,
            'warehouse': self.location.id,
            'delivery_address': self.patient.name.addresses[0].id,
            'moves': [('add', moves)],
        })
        if to_create:
            Shipment.create(to_create)

    def get_stock_moves(self, medications):
        Move = Pool().get('stock.move')
        moves = []
        for medication in medications:
            move_info = {}
            move_info['origin'] = str(self)
            move_info['product'] = medication.medicament.name.id
            move_info['uom'] = medication.medicament.name.default_uom.id
            move_info['quantity'] = medication.qty
            move_info['from_location'] = self.location.output_location.id
            move_info['to_location'] = self.patient.name.customer_location.id
            move_info['unit_price'] = medication.medicament.name.list_price
            move_info['state'] = 'draft'
            moves.append(move_info)
        new_moves = Move.create(moves)
        return new_moves

    def create_hospitalization(self):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')
        now = datetime.now()
        tomorrow = now + timedelta(days=1)
        values = {
            'patient': self.patient.id,
            'admission_type': 'emergency',
            'emergency': self.id,
            'hospitalization_date': now,
            'discharge_date': tomorrow,
            'state': 'hospitalized',
            'institution': self.institution.id,
        }
        Inpatient.create([values])


class VitalSigns(ModelSQL, ModelView):
    'Vital Signs'
    __name__ = 'gnuhealth.emergency.vital_signs'

    emergency = fields.Many2One('gnuhealth.emergency',
        'Emergency Code')
    blood_pressure = fields.Char('Blood Pressure (mm/Hg)',
        help='Reference Values: \n'
            'Normal Values: Sistólica: 116+/-12 Diastólica: 70+/-7 \n'
            'Minimal Values: Sistólica: 90 Diastólica: 60 \n'
            'Alert Values: Sistólica: 130 Diastólica: 90 \n'
            'Emergency Values: Sistólica: 160 Diastólica: 110')
    heart_rate = fields.Integer(
        'Heart Rate', help='Heart rate expressed in beats per minute')
    temperature = fields.Float(
        'Temperature',
        help='Temperature in celcius')
    respiratory_rate = fields.Integer(
        'Respiratory Rate',
        help='Respiratory rate expressed in breaths per minute')
    osat = fields.Integer(
        'Oxygen Saturation',
        help='Arterial oxygen saturation expressed as a percentage')
    heart_rate_fetal = fields.Integer(
        'FCF', help='Heart rate fetal expressed in beats per minute')
    glycemia = fields.Integer(
        'Glycemia', help='levels of the glucose on blood ')
    taking_date = fields.DateTime('Taking Date',
        help='Date of the taking of samples')


class ToxicHabits(ModelSQL, ModelView):
    'Toxic Habits'
    __name__ = 'gnuhealth.emergency.toxic_habits'

    emergency = fields.Many2One('gnuhealth.emergency',
        'Emergency Code')
    habit = fields.Selection(TOXIC_HABITS, 'Habit', required=True)
    habit_string = habit.translated('habit')
    quantity = fields.Numeric('Quantity', required=True)
    frequency = fields.Char('Frequency', required=True)


class NoteList(ModelSQL, ModelView):
    'Note List'
    __name__ = 'gnuhealth.emergency.note_list'

    emergency = fields.Many2One('gnuhealth.emergency',
        'Emergency Code')
    date_note = fields.DateTime('Date Note', readonly=True)
    attending_physician = fields.Many2One('gnuhealth.healthprofessional',
        'Attending Physician', required=True)
    note = fields.Text('Note')

    @staticmethod
    def default_date_note():
        return datetime.now()

    @staticmethod
    def default_attending_physician():
        print(Transaction().context.get('user'))
        return None


class Interconsultation(ModelSQL, ModelView):
    'Interconsultation'
    __name__ = 'gnuhealth.emergency.interconsultations'

    emergency = fields.Many2One('gnuhealth.emergency',
        'Emergency Code')
    specialty = fields.Many2One('gnuhealth.specialty', 'Specialty', required=True)
    attending_physician = fields.Many2One('gnuhealth.healthprofessional',
        'Attending Physician', required=True)
    cause = fields.Text('Note', required=True)


class Lab(metaclass=PoolMeta):
    __name__ = 'gnuhealth.lab'
    emergency = fields.Many2One('gnuhealth.emergency', 'Emergency')


class ImagingTestRequest(metaclass=PoolMeta):
    __name__ = 'gnuhealth.imaging.test.request'
    emergency = fields.Many2One('gnuhealth.emergency', 'Emergency')


class EmergencyProcedures(ModelSQL, ModelView):
    'Emergency Procedures'
    __name__ = 'gnuhealth.emergency.procedures'
    emergency = fields.Many2One('gnuhealth.emergency', 'Emergency')
    date_procedure = fields.DateTime('Date', required=True)
    procedure = fields.Many2One('gnuhealth.procedure', 'procedure', required=True)


class MedicalReleaseReport(Report):
    'Medical Release Report'
    __name__ = 'emergency.medical_release_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MedicalReleaseReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        time_now = datetime.now() - timedelta(hours=5)
        report_context['hour'] = time_now
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class ReferredPatientReport(Report):
    'Referred Patient Report'
    __name__ = 'emergency.referred_patient_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReferredPatientReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        time_now = datetime.now() - timedelta(hours=5)
        report_context['hour'] = time_now
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class EmergencyMedicalHistory(Report):
    'Emergency Medical History'
    __name__ = 'gnuhealth.emergency.history_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(EmergencyMedicalHistory, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class CreateEmergencyShipment(Wizard):
    """
    Create Emergency Shipment
    """
    __name__ = 'gnuhealth.emergency.create_shipment'
    start_state = 'generate_shipment'
    generate_shipment = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateEmergencyShipment, cls).__setup__()
        cls._error_messages.update({
            'dont_shipment_generated': (
                'You can not to generate shipment!'
            ),
        })

    def transition_generate_shipment(self):
        Emergency = Pool().get('gnuhealth.emergency')
        ids = Transaction().context['active_ids']
        if ids:
            for record in Emergency.browse(ids):
                if record.state in ('draft', 'admitted'):
                    record.generate_shipment()
        return 'end'


class EmergencyMedication(ModelSQL, ModelView):
    'Emergency Medication'
    __name__ = 'gnuhealth.emergency.medication'
    emergency = fields.Many2One('gnuhealth.emergency', 'Emergency')
    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        required=True, help='Prescribed Medicament')
    qty = fields.Integer('Quantity', required=True,
        help='Quantity of units (eg, 2 capsules) of the medicament')
    indication = fields.Many2One('gnuhealth.pathology', 'Indication',
        help='Choose a disease for this medicament from the disease list. It'
        ' can be an existing disease of the patient or a prophylactic.')
    application_date = fields.Date('Application Date',
        help='Date of application', required=True)
    dose = fields.Float('Dose',
        help='Amount of medication (eg, 250 mg) per dose', required=True)
    dose_unit = fields.Many2One('gnuhealth.dose.unit', 'Dose Unit',
        required=True, help='Unit of measure for the medication to be taken')
    route = fields.Many2One('gnuhealth.drug.route', 'Administration Route',
        required=True, help='Drug administration route code.')
    form = fields.Many2One('gnuhealth.drug.form', 'Form', required=True,
        help='Drug form, such as tablet or gel')
    adverse_reaction = fields.Text('Adverse Reactions',
        help='Side effects or adverse reactions that the patient experienced')


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + [
            'gnuhealth.emergency',
        ]


class InpatientRegistration(metaclass=PoolMeta):
    __name__ = 'gnuhealth.inpatient.registration'
    emergency = fields.Many2One('gnuhealth.emergency', 'Emergency', states={
        'readonly': True,
    })
