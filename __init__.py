# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2021 Oscar Alvarez <oscar.alvarez.montero@gmail.org>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.pool import Pool
import configuration
import emergency


def register():
    Pool.register(
        configuration.HealthConfiguration,
        emergency.ImagingTestRequest,
        emergency.Emergency,
        emergency.VitalSigns,
        emergency.ToxicHabits,
        emergency.NoteList,
        emergency.Interconsultation,
        emergency.Lab,
        emergency.Move,
        emergency.EmergencyProcedures,
        emergency.EmergencyMedication,
        emergency.InpatientRegistration,
        module='health_emergency', type_='model')
    Pool.register(
        emergency.MedicalReleaseReport,
        emergency.ReferredPatientReport,
        emergency.EmergencyMedicalHistory,
        module='health_emergency', type_='report')
    Pool.register(
        emergency.CreateEmergencyShipment,
        module='health_emergency', type_='wizard')
